#include <exception>
#include "tictactoe.h"

TicTacToe::TicTacToe(IPlayer* playerX, IPlayer* playerO)
    : _board{ {Element{}, Element{}, Element{}},
            {Element{}, Element{}, Element{}},
            {Element{}, Element{}, Element{}}}
    , _playerX{playerX}
    , _playerO{playerO}
    , _nextToMove{PlayerMove::X}
    , _moveCount{0}
{
    _playerX->setTicTacToeGame(this);
    _playerO->setTicTacToeGame(this);
}

std::vector<TicTacToe::Position> TicTacToe::getLegalMoves() const
{
    std::vector<TicTacToe::Position> legalMoves{};

    for (auto i = 0; i < 3; i++) {
        for (auto j = 0; j < 3; j++) {
            if (!_board[i][j].has_value()) {
                legalMoves.push_back(TicTacToe::Position{i, j});
            }
        }
    }

    return legalMoves;
}

void TicTacToe::startGame()
{
    _playerX->makeTheMove();
}

void TicTacToe::play(Position p) {
    validateMove(p);
    _board[p.first][p.second] = _nextToMove;
    _moveCount++;

    auto winner = checkWinner(_board);
    if (winner.has_value() && winner.value() == PlayerMove::X) {
        _playerX->youWin();
        _playerO->youLose();
    }
    else if (winner.has_value() && winner.value() == PlayerMove::O) {
        _playerO->youWin();
        _playerX->youLose();
    }
    else if (_moveCount == 9) {
        _playerO->gameEndedTied();
        _playerX->gameEndedTied();
    }
    else {
        if (_nextToMove == TicTacToe::PlayerMove::X) {
            _nextToMove = TicTacToe::PlayerMove::O;
            _playerO->makeTheMove();
        }
        else {
            _nextToMove = TicTacToe::PlayerMove::X;
            _playerX->makeTheMove();
        }
    }
}

TicTacToe::Board TicTacToe::getBoard() const
{
    return _board;
}

std::optional<TicTacToe::PlayerMove> TicTacToe::checkWinner(const TicTacToe::Board &board)
{
    std::optional<TicTacToe::PlayerMove> winner{};

    // check rows
    for (auto i = 0; i < 3; i++) {
        if (board[i][0].has_value() && board[i][1].has_value() && board[i][2].has_value() &&
                board[i][0].value() == board[i][1].value() && board[i][1].value() == board[i][2].value()){
            if (winner.has_value() && winner.value() != board[i][0].value()) {
                // Illegal state!
                throw std::runtime_error("Board is in the illegal state");
            }
            winner = board[i][0];
        }
    }

    // check columns
    for (auto i = 0; i < 3; i++) {
        if (board[0][i].has_value() && board[1][i].has_value() && board[2][i].has_value() &&
                board[0][i].value() == board[1][i].value() && board[1][i].value() == board[2][i].value()){
            if (winner.has_value() && winner.value() != board[0][i].value()) {
                // Illegal state!
                throw std::runtime_error("Board is in the illegal state");
            }
            winner = board[0][i];
        }
    }

    // check diagonals
    if (board[0][0].has_value() && board[1][1].has_value() && board[2][2].has_value() &&
            board[0][0].value() == board[1][1].value() && board[1][1].value() == board[2][2].value())
    {
        if (winner.has_value() && winner.value() != board[1][1].value()) {
            // Illegal state!
            throw std::runtime_error("Board is in the illegal state");
        }
        winner = board[1][1];
    }

    if (board[0][2].has_value() && board[1][1].has_value() && board[2][0].has_value() &&
            board[0][2].value() == board[1][1].value() && board[1][1].value() == board[2][0].value())
    {
        if (winner.has_value() && winner.value() != board[1][1].value()) {
            // Illegal state!
            throw std::runtime_error("Board is in the illegal state");
        }
        winner = board[1][1];
    }

    return winner;
}

void TicTacToe::validateMove(Position p) const {
    if (p.first < 0 || p.first > 2 || p.second < 0 || p.second > 2) {
        throw std::out_of_range("Invalid position");
    }

    if (_board[p.first][p.second].has_value()) {
        throw std::runtime_error("The move has already been played at this position");
    }
}

