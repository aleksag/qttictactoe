#include <limits>
#include "cpuplayer.h"

CpuPlayer::CpuPlayer(unsigned int maxSearchDepth)
    : _maxSearchDepth(maxSearchDepth)
{
}

void CpuPlayer::setTicTacToeGame(TicTacToe *ticTacToe) {
    _ticTacToe = ticTacToe;
}

void CpuPlayer::makeTheMove() {
    auto board = _ticTacToe->getBoard();
    auto legalMoves = _ticTacToe->getLegalMoves();
    if (!_myMove.has_value())
    {
        // deduce if we are X or O
        _myMove.emplace(legalMoves.size() % 2 == 0 ? TicTacToe::PlayerMove::O : TicTacToe::PlayerMove::X);
        _opponentsMove.emplace(legalMoves.size() % 2 == 1 ? TicTacToe::PlayerMove::O : TicTacToe::PlayerMove::X);
    }

    auto position = getTheBestMove(board, legalMoves);
    _ticTacToe->play(position);
}

TicTacToe::Position CpuPlayer::getTheBestMove(TicTacToe::Board board, std::vector<TicTacToe::Position> legalMoves) const
{
    if (legalMoves.size() == 0) {
        throw std::runtime_error("There must be legal moves at this stage.");
    }

    auto maxScore = std::numeric_limits<int>::min();
    TicTacToe::Position bestPosition = TicTacToe::Position{};
    for (unsigned int i = 0; i < legalMoves.size(); i++) {
        auto position = legalMoves[i];
        auto remainingMoves = legalMoves;
        remainingMoves.erase(remainingMoves.begin() + i);
        auto boardCopy = board;
        boardCopy[position.first][position.second] = _myMove;
        int score = minimax(0, false, boardCopy, remainingMoves);
        if (score > maxScore) {
            maxScore = score;
            bestPosition = position;
        }
    }

    return bestPosition;
}

int CpuPlayer::minimax(unsigned int depth, bool isMaximizing, TicTacToe::Board board, std::vector<TicTacToe::Position> legalMoves) const
{
    auto currentScore = scoreTheState(board);
    if (currentScore != 0 || legalMoves.size() == 0 || depth >= _maxSearchDepth)
    {
        return currentScore;
    }

    if (isMaximizing) {
        auto maxScore = std::numeric_limits<int>::min();;
        for (unsigned int i = 0; i < legalMoves.size(); i++) {
            auto position = legalMoves[i];
            auto remainingMoves = legalMoves;
            remainingMoves.erase(remainingMoves.begin() + i);
            auto boardCopy = board;
            boardCopy[position.first][position.second] = _myMove;
            int score = minimax(depth + 1, false, boardCopy, remainingMoves);
            if (score > maxScore) {
                maxScore = score;
            }
        }
        return maxScore;
    }
    else {
        auto minScore = std::numeric_limits<int>::max();
        for (unsigned int i = 0; i < legalMoves.size(); i++) {
            auto position = legalMoves[i];
            auto remainingMoves = legalMoves;
            remainingMoves.erase(remainingMoves.begin() + i);
            auto boardCopy = board;
            boardCopy[position.first][position.second] = _opponentsMove;
            int score = minimax(depth + 1, true, boardCopy, remainingMoves);
            if (score < minScore) {
                minScore = score;
            }
        }
        return minScore;
    }
}

int CpuPlayer::scoreTheState(const TicTacToe::Board &b) const
{
    auto winner = TicTacToe::checkWinner(b);
    if (!winner.has_value())
    {
        return 0;
    }
    else if (winner.value() == _myMove.value())
    {
        return 1;
    }
    else {
        return -1;
    }
}
