#ifndef CPUPLAYER_H
#define CPUPLAYER_H

#include<map>
#include "tictactoe.h"

class CpuPlayer : public TicTacToe::IPlayer
{
public:
    CpuPlayer(unsigned int maxSearchDepth);
    virtual void setTicTacToeGame(TicTacToe *) override;
    virtual void makeTheMove() override;

private:

    TicTacToe::Position getTheBestMove(TicTacToe::Board board, std::vector<TicTacToe::Position> legalMoves) const;
    int minimax(unsigned int depth, bool isMaximizing, TicTacToe::Board, std::vector<TicTacToe::Position> legalMoves) const;
    int scoreTheState(const TicTacToe::Board& b) const;

    TicTacToe* _ticTacToe;
    std::optional<TicTacToe::PlayerMove> _myMove;
    std::optional<TicTacToe::PlayerMove> _opponentsMove;
    unsigned int _maxSearchDepth;
};

#endif // CPUPLAYER_H
