#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _humanPlaysFirst = ui->actionHuman_plays_first->isChecked();
    ui->frame->setDisabled(true);
    ui->statusbar->showMessage("Please start the game.");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::renderBoard() {
    auto board = _ticTacToe->getBoard();
    ui->pushButton0_0->setText(boardElementToString(board[0][0]));
    ui->pushButton0_1->setText(boardElementToString(board[0][1]));
    ui->pushButton0_2->setText(boardElementToString(board[0][2]));
    ui->pushButton1_0->setText(boardElementToString(board[1][0]));
    ui->pushButton1_1->setText(boardElementToString(board[1][1]));
    ui->pushButton1_2->setText(boardElementToString(board[1][2]));
    ui->pushButton2_0->setText(boardElementToString(board[2][0]));
    ui->pushButton2_1->setText(boardElementToString(board[2][1]));
    ui->pushButton2_2->setText(boardElementToString(board[2][2]));

}

QString MainWindow::boardElementToString(const TicTacToe::Element e)
{
    if (!e.has_value()) {
        return "";
    }
    else if (e.value() == TicTacToe::PlayerMove::X) {
        return "X";
    }
    else {
        return "O";
    }
}

void MainWindow::makeTheMove() {
    // user's turn now, rerender the state of the game
    renderBoard();
}

void MainWindow::setTicTacToeGame(TicTacToe *) {
    // do nothing, we already have a reference
}

void MainWindow::youWin()
{
    renderBoard();
    ui->statusbar->showMessage("Hurray!!! You won!\nYou are very smart!");
    ui->frame->setDisabled(true);
}

void MainWindow::youLose()
{
    renderBoard();
    ui->statusbar->showMessage("Oh no!\nLost to a CPU??? This sucks...");
    ui->frame->setDisabled(true);
}

void MainWindow::gameEndedTied()
{
    renderBoard();
    ui->statusbar->showMessage("Tied... Such a boring game.");
    ui->frame->setDisabled(true);
}



void MainWindow::newGame(unsigned int cpuPlayerSearchDepth) {
    _cpuPlayer.reset(new CpuPlayer(cpuPlayerSearchDepth));
    if (_humanPlaysFirst) {
        _ticTacToe.reset(new TicTacToe(this, _cpuPlayer.get()));
    }
    else {
        _ticTacToe.reset(new TicTacToe(_cpuPlayer.get(), this));
    }
    ui->frame->setDisabled(false);
    renderBoard();
    _ticTacToe->startGame();
    ui->statusbar->clearMessage();
}

void MainWindow::handleUserMove(TicTacToe::Position p)
{
    try {
        _ticTacToe->play(p);
    } catch (...) {
        ui->statusbar->showMessage("Not a valid move", 3000);
    }

    renderBoard();
}

void MainWindow::on_pushButton0_0_clicked()
{
    handleUserMove(TicTacToe::Position{0, 0});
}

void MainWindow::on_pushButton0_1_clicked()
{
    handleUserMove(TicTacToe::Position{0, 1});
}

void MainWindow::on_pushButton0_2_clicked()
{
    handleUserMove(TicTacToe::Position{0, 2});
}

void MainWindow::on_pushButton1_0_clicked()
{
    handleUserMove(TicTacToe::Position{1, 0});
}

void MainWindow::on_pushButton1_1_clicked()
{
    handleUserMove(TicTacToe::Position{1, 1});
}

void MainWindow::on_pushButton1_2_clicked()
{
    handleUserMove(TicTacToe::Position{1, 2});
}

void MainWindow::on_pushButton2_0_clicked()
{
    handleUserMove(TicTacToe::Position{2, 0});
}

void MainWindow::on_pushButton2_1_clicked()
{
    handleUserMove(TicTacToe::Position{2, 1});
}

void MainWindow::on_pushButton2_2_clicked()
{
    handleUserMove(TicTacToe::Position{2, 2});
}

void MainWindow::on_actionHuman_plays_first_toggled(bool arg1)
{
    _humanPlaysFirst = arg1;
}

void MainWindow::on_actionEasy_triggered()
{
    newGame(0);
}

void MainWindow::on_actionHard_triggered()
{
    newGame(9);
}
