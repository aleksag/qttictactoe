#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <memory>

#include "tictactoe.h"
#include "cpuplayer.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow, public TicTacToe::IPlayer
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public:
    // TicTacToe::IPlayer interface implementation
    virtual void makeTheMove() override;
    virtual void setTicTacToeGame(TicTacToe *) override;
    virtual void youWin() override;
    virtual void youLose() override;
    virtual void gameEndedTied() override;

private slots:
    void on_pushButton0_0_clicked();
    void on_pushButton0_1_clicked();
    void on_pushButton0_2_clicked();
    void on_pushButton1_0_clicked();
    void on_pushButton1_1_clicked();
    void on_pushButton1_2_clicked();
    void on_pushButton2_0_clicked();
    void on_pushButton2_1_clicked();
    void on_pushButton2_2_clicked();
    void on_actionHuman_plays_first_toggled(bool arg1);
    void on_actionEasy_triggered();
    void on_actionHard_triggered();

private:
    void newGame(unsigned int cpuPlayerSearchDepth);
    void renderBoard();
    void handleUserMove(TicTacToe::Position);
    static QString boardElementToString(const TicTacToe::Element);

    Ui::MainWindow *ui;
    std::unique_ptr<TicTacToe>  _ticTacToe;
    std::unique_ptr<CpuPlayer>  _cpuPlayer;

    // some settings
    bool _humanPlaysFirst;
};
#endif // MAINWINDOW_H
