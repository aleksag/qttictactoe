#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <utility>
#include <optional>
#include <vector>
#include <memory>

class TicTacToe
{
public:
    /**
     * @brief The IPlayer class
     * The player of the TicTacToe should implement this interface
     */
    class IPlayer {
    public:
        /**
         * @brief setTicTacToeGame
         * this method is used to inject the pointer to the TicTacToe
         * game to the implemented player
         */
        virtual void setTicTacToeGame(TicTacToe*) = 0;
        /**
         * @brief makeTheMove
         * informs the player that it is his turn to play
         */
        virtual void makeTheMove() = 0;
        /**
         * @brief youWin informs the player that he won
         */
        virtual void youWin() {};
        /**
         * @brief youLose informs the player that he lost
         */
        virtual void youLose() {};
        /**
         * @brief youLose informs the player that he lost
         */
        virtual void gameEndedTied() {};

    };

    enum class PlayerMove {
        X,
        O,
    };

    typedef std::optional<PlayerMove> Element;
    typedef std::vector<std::vector<Element>> Board;
    typedef std::pair<int, int> Position;


    TicTacToe(IPlayer* playerX, IPlayer* playerO);
    void startGame();

    // Methods used by TicTacToe::IPlayer
    void play(Position);
    std::vector<Position> getLegalMoves() const;
    Board getBoard() const;
    static std::optional<PlayerMove> checkWinner(const Board& board);

private:
    void validateMove(Position) const;

    Board         _board;
    IPlayer*      _playerX;
    IPlayer*      _playerO;

    PlayerMove   _nextToMove;
    unsigned int _moveCount;
};

#endif // TICTACTOE_H
